# AMHash

Šioje repozitorijoje pateikiamas mano sukurtas hash generatorius. Generatorius parašytas C++ kalba CLion aplinkoje laikantis  C++17 standartų. Šis kodas parašytas VU MIF ISI studijų programos pasirenkamojo dalyko "Blokų grandinių technologijos" [1-ai užduočiai](https://github.com/blockchain-group/Blockchain-technologijos/blob/master/pratybos/1uzduotis-Hashavimas.md) atlikti.

## Reikalavimai generatoriui
- Pagal užduoties formuluotę, realizuokite *hash*'ų generatorių (pageidautina `C++` kalboje). Programos realizavimas turi būti versijuojamas (pageidautina *git*'e) ir patalpintas Jūsų Github'e.
- Realizacijoje *input*'ą, esantį išoriniame faile, reikia nurodyti per pateikta `Command Line Argument`'ą. Tačiau turi būti galimybė paduoti ir simbolių eilutę, aka `string`'ą.
- Atlikite eksperimentinę analizę (žr. Komentarai dėl pradinio eksperimentinio tyrimo-analizės), kurios metu įsitikinkite, kad Jūsų *hash* funkcija-generatorius iš tiesų pasižymi aukščiau aprašytais *hash* funkcijoms keliamais reikalavimais. Atliktą tyrimą išsamiai aprašykite `README` faile.
- Eksperimentinis tyrimas-analizė turi būti atkartojamas, t. y., paskaitos metu reikės pademonstruoti, kaip vyksta tyrimas ir kaip argumentavote, kad Jūsų maišos funkcija pasižymi šiomis savybėmis.
- Apraytikite savo funkcijos idėją ir eksperimentinio tyrimo vykdymą ir rezultatus ataskaitoje.
## Analizė
Pirmoji dalis

|m|h(m)|
|-|----|
|a|34c0000000000061|
|b|a180000000000062|
|2-1.txt (daugiau nei 10000 simbolių)|d89329051036bc80|
|2-2.txt (daugiau nei 10000 simbolių)|e555018bf4114dfb|
|3-1.txt (daugiau nei 10000 simbolių)|853330a90d5dc153|
|3-2.txt(3-1.txt su vienu pakeistu simboliu)|c872c5b04889b993|


* Kontitucijai suhashashinti vidutiniškai prireikė: 0.0023431 s
* Atsitiktinių porų hash`ų sutapimų skaičius:

|Simbolių eilutės ilgis| Kolizijų skaičius|
|----------------------|------------------|
|5|0 iš 1 000 000 |
|50|0 iš 1 000 000|
|100|0 iš 1 000 000 |
|1000|0 iš 1 000 000 |

* During the generation of 100000 of not equal random pairs of strings that differ by one character we found that:

|Simbolių eilutės ilgis| Maksimalus skirtingumas| Minimalus  skirtingumas | Vidutinis skirtingumas|
|----------------------|------------------------|-------------------------|-----------------------|
|5|81% |23% |50%|
|50|76% |23% |50%|
|100|76% |25% |49%|
|1000|78% |20% |49%|
|Average|78% |23% |50%|

## Changelog
### v0.1.1 - 2019/10/10
#### Atnaujinta
* Sumažintas kolizijų skaičius
* Sukurto generatoriaus testinė analizė
* README failas, kuriame aprašomas tyrimas
### v0.1 - 2019/10/03
#### Pridėta
* Veikiantis hash generatorius.
* Sukurto generatoriaus testinė analizė
* README failas, kuriame aprašomas tyrimas
