#include <iostream>
#include <fstream>
#include <random>
#include <vector>
#include <chrono>
#include <algorithm>
#include <bitset>
#include <cstdio>
#include <stdexcept>
#include <sstream>

using namespace std::chrono;
using std::string;

string amHash(const string& input);
string make64Bit(unsigned long long charSum, size_t length);

string kurkEilute(size_t ilgis){
    auto randchar = []() -> char
    {
        const char charset[] =
                "0123456789"
                "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                "abcdefghijklmnopqrstuvwxyz";
        const size_t max_index = (sizeof(charset) - 1);
        return charset[ rand() % max_index ];
    };
    std::string str(ilgis,0);
    std::generate_n( str.begin(), ilgis, randchar );
    return str;
}

string kurkOCEilute(string pirma){
    string n = kurkEilute(1);
    int pos = rand();
    pirma[pos % pirma.length()] = n[0];
    return pirma;
}

bool patikrinkIlgi(size_t ilgis){
    string s1 = kurkEilute(ilgis);
    string s2 = kurkEilute(ilgis);
    string h1 = amHash(s1);
    string h2 = amHash(s2);
    if (s1.length() == s2.length()){
        std::cout << "When hashing messages with the length of " << ilgis << " the length of hashes match." << std::endl;
        return true;
    } else {
        std::cout << "When hashing messages with the length of " << ilgis << " the length of hashes does not match. Not running further tests."
                  << std::endl;
        return false;
    }
}

void konstitututita() {
    unsigned long pilnasLaikas = 0;
    std::ifstream in("../konstitucija.txt");
    std::string str;
    std::vector<string> kons{};

    while (std::getline(in, str)) {
        kons.push_back(str);
    }

    std::cout << "Running konstitucija test:" << std::endl;
    for (int i = 0; i < 10000; i++) {
        auto start = high_resolution_clock::now();
        for (const string& s : kons) {
            amHash(s);
        }
        auto stop = high_resolution_clock::now();
        auto duration = duration_cast<milliseconds>(stop - start);
        pilnasLaikas += duration.count();
    }

    double time =  pilnasLaikas * 1.0 / 1000.0 / 10000;
    std::cout << "The average time to hash Kontitucija: " << time  << " s" << std::endl;
}

void eilutes(int iterations){
    std::cout << "Starting words test." << std::endl;
    int sutapimai = 0;
    for (int i = 0; i < iterations; i++){
        string s1 = kurkEilute(5);
        string s2 = kurkEilute(5);
        if (s1 != s2) {
            string h1 = amHash(s1);
            string h2 = amHash(s2);
            if (h1 == h2) {
                sutapimai++;
            }
        } else {
            i--;
            continue;
        }
        if (i % (iterations / 10) == 0) {
            std::cout << i * 100 / iterations << "% " << std::endl;
        }
    }
    std::cout << std::endl;
    std::cout << "During the generation of " << iterations << " of not equal random pairs of strings we found "
              << sutapimai << " pairs that have the same hash." << std::endl;
}

void raides(int iterations){
    std::cout << "Starting bitwise test." << std::endl;
    int max = -100;
    string max1;
    string max2;

    int min = 150;
    string min1;
    string min2;

    int bitcount = 0;
    unsigned int sum = 0;

    for (size_t i = 0; i < iterations; i++){
        int t = 0;
        string s1 = kurkEilute(1000);
        string s2 = kurkOCEilute(s1);

        if (s1 == s2) {
            i--;
            continue;
        }

        string h1 = amHash(s1);
        string h2 = amHash(s2);
        string b1;
        string b2;
        for (char c : h1){
            int a;
            std::stringstream ss;
            ss << std::hex << c;
            ss >> a;
            b1 += std::bitset<4>(a).to_string();
        }
        for (char c : h2){
            int a;
            std::stringstream ss;
            ss << std::hex << c;
            ss >> a;
            b2 += std::bitset<4>(a).to_string();
        }
        bitcount = b1.length();

        for (size_t j = 0; j < b1.length(); j++){
            if (b1[j] != b2[j]) {
                t++;
            }
        }
        if (t > max) {
            max = t;
            max1 = s1;
            max2 = s2;
        }
        if (t < min) {
            min = t;
            min1 = s1;
            min2 = s2;
        }
        sum += t;
        if (i % (iterations / 10) == 0) {
            std::cout << i * 100 / iterations << "% " << std::endl;
        }
    }
    std::cout << std::endl;
    std::cout << "During the generation of " << iterations << " of not equal random pairs of strings that differ by one character we found that:" << std::endl;
    std::cout << "The maximum bitwise difference is " << max * 100 / bitcount << "%." << std::endl;
    std::cout << "The minimum bitwise difference is " << min * 100 / bitcount << "%." << std::endl;
    std::cout << "The average bitwise difference is " << (sum * 100 / bitcount) / iterations << "%." << std::endl;
}

void analize(){

    srand(time(0));

    if (!patikrinkIlgi(1)){
        return;
    }
    if (!patikrinkIlgi(1000)){
        return;
    }
    konstitututita();
    eilutes(1000000);
    raides(100000);
}

int main(int argc,char* argv[]) {
    string mode = argv[1];
    if (mode == "--test"){
        analize();
        return 0;
    }
    if (mode == "--file"){
        std::ifstream in(argv[2]);
        std::string content( (std::istreambuf_iterator<char>(in) ),
                             (std::istreambuf_iterator<char>()    ) );
        std::cout << amHash(content) << std::endl;
        return 0;
    }
    std::cout << amHash(argv[1]) << std::endl;
    return 0;
}

string amHash(const string& input) {
    if (input.empty()) {
        std::stringstream ss;
        ss << std::hex << 1152921504606846977;
        return ss.str();
    } else {
        unsigned long long sum = 0;
        int mult = input.size();
        for (char i : input) {
            int t = (int) i;
            sum += t * mult;
            mult--;
        }

        string bit64 = make64Bit(sum, input.size() + 1);
        unsigned long long int int64 = 0;
        std::stringstream ss;
        ss << std::hex << bit64;
        ss >> int64;

        unsigned long long int xmult = 1099511628211;

        for (unsigned char i : input){
            int64 *= xmult;
            int64 ^= i;
        }

        std::stringstream sss;
        sss << std::hex << int64;
        string hash = sss.str();
        return hash;
    }
}
string make64Bit(unsigned long long charSum, size_t length) {
    if (charSum <= 1152921504606846977) {
        charSum *= length;
        return make64Bit(charSum, length);
    } else {
        std::stringstream ss;
        ss << std::hex << charSum;
        string s = ss.str();
        return s.substr(0, 16);
    }
}
